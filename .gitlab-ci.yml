include: 
    - template: 'Auto-DevOps.gitlab-ci.yml'

variables:
    TEST_DISABLED: 'true'
    REVIEW_DISABLED: 'true'
    DAST_DISABLED: 'true'
    BROWSER_PERFORMANCE_DISABLED: 'true'

.gemnasium-shared-rule:
  changes:
    - '**/Gemfile.lock'
    - '**/composer.lock'
    - '**/gems.locked'
    - '**/go.sum'
    - '**/npm-shrinkwrap.json'
    - '**/package-lock.json'
    - '**/yarn.lock'
    - '**/pnpm-lock.yaml'
    - '**/packages.lock.json'
    - '**/conan.lock'

gemnasium-dependency_scanning:
  rules:
    - if: $DEPENDENCY_SCANNING_DISABLED == 'true' || $DEPENDENCY_SCANNING_DISABLED == '1'
      when: never
    - if: $DS_EXCLUDED_ANALYZERS =~ /gemnasium([^-]|$)/
      when: never
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdependency_scanning\b/ &&
          $CI_GITLAB_FIPS_MODE == "true"
      changes: !reference [.gemnasium-shared-rule, changes]
      variables:
        DS_IMAGE_SUFFIX: "-fips"
        DS_REMEDIATE: "false"
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdependency_scanning\b/
      changes: !reference [.gemnasium-shared-rule, changes]



.gemnasium-python-shared-rule:
  changes:
    - '**/requirements.txt'
    - '**/requirements.pip'
    - '**/Pipfile'
    - '**/Pipfile.lock'
    - '**/requires.txt'
    - '**/setup.py'
    - '**/poetry.lock'

gemnasium-python-dependency_scanning:
  rules:
    - if: $DEPENDENCY_SCANNING_DISABLED == 'true' || $DEPENDENCY_SCANNING_DISABLED == '1'
      when: never
    - if: $DS_EXCLUDED_ANALYZERS =~ /gemnasium-python/
      when: never
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdependency_scanning\b/ &&
          $CI_GITLAB_FIPS_MODE == "true"
      changes: !reference [.gemnasium-python-shared-rule, changes]
      variables:
        DS_IMAGE_SUFFIX: "-fips"
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdependency_scanning\b/
      changes: !reference [.gemnasium-python-shared-rule, changes]
    # Support passing of $PIP_REQUIREMENTS_FILE
    # See https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#configuring-specific-analyzers-used-by-dependency-scanning
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdependency_scanning\b/ &&
          $PIP_REQUIREMENTS_FILE &&
          $CI_GITLAB_FIPS_MODE == "true"
      variables:
        DS_IMAGE_SUFFIX: "-fips"
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdependency_scanning\b/ &&
          $PIP_REQUIREMENTS_FILE

.gemnasium-maven-shared-rule:
  changes:
    - '**/build.gradle'
    - '**/build.gradle.kts'
    - '**/build.sbt'
    - '**/pom.xml'

gemnasium-maven-dependency_scanning:
  rules:
  - when: never

gemnasium-maven-dependency_scanning-adservice:
  extends:
    - gemnasium-maven-dependency_scanning
    - .ds-analyzer
    - .cyclonedx-reports
  variables:
    DS_ANALYZER_NAME: "gemnasium-maven"
    SECURE_LOG_LEVEL: "debug"
  rules:
    - if: $DEPENDENCY_SCANNING_DISABLED == 'true' || $DEPENDENCY_SCANNING_DISABLED == '1'
      when: never
    - if: $DS_EXCLUDED_ANALYZERS =~ /gemnasium-maven/
      when: never
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdependency_scanning\b/ &&
          $CI_GITLAB_FIPS_MODE == "true"
      changes: !reference [.gemnasium-maven-shared-rule, changes]
      variables:
        DS_IMAGE_SUFFIX: "-fips"
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdependency_scanning\b/
      changes: !reference [.gemnasium-maven-shared-rule, changes]


build:
  before_script:
  - cd src/${MICROSERVICE} && pwd
  variables:
    CI_APPLICATION_REPOSITORY: "${CI_REGISTRY_IMAGE}/${MICROSERVICE}-${CI_COMMIT_REF_SLUG}"
  rules:
  - when: never

.build_rules:
  rules:
  - if: '$BUILD_DISABLED'
    when: never
  - if: '$AUTO_DEVOPS_PLATFORM_TARGET == "EC2"'
    when: never
  

build-emailservice:
  extends: build
  rules:
  - !reference [.build_rules, rules]
  - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'
    changes: 
    - 'src/emailservice/**/*'
    variables:
      MICROSERVICE: emailservice

build-adservice:
  extends: build
  rules:
  - !reference [.build_rules, rules]
  - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'
    changes: 
    - 'src/adservice/**/*'
    variables:
      MICROSERVICE: adservice

build-cartservice:
  extends: build
  before_script:
  - cd src/${MICROSERVICE}/src
  rules:
  - !reference [.build_rules, rules]
  - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'
    changes: 
    - 'src/cartservice/**/*'
    variables:
      MICROSERVICE: cartservice

build-checkoutservice:
  extends: build
  rules:
  - !reference [.build_rules, rules]
  - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'
    changes: 
    - 'src/checkoutservice/**/*'
    variables:
      MICROSERVICE: checkoutservice

build-currencyservice:
  extends: build  
  rules:
  - !reference [.build_rules, rules]
  - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'
    changes: 
    - 'src/currencyservice/**/*'
    variables:
      MICROSERVICE: currencyservice             

build-frontend:
  extends: build   
  rules:
  - !reference [.build_rules, rules]
  - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'
    changes: 
    - 'src/frontend/**/*'
    variables:
      MICROSERVICE: frontend    

build-paymentservice:
  extends: build
  rules:
  - !reference [.build_rules, rules]
  - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'
    changes: 
    - 'src/paymentservice/**/*'
    variables:
      MICROSERVICE: paymentservice        

build-productcatalogservice:
  extends: build
  rules:
  - !reference [.build_rules, rules]
  - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'
    changes: 
    - 'src/productcatalogservice/**/*'
    variables:
      MICROSERVICE: productcatalogservice    

build-recommendationservice:
  extends: build
  rules:
  - !reference [.build_rules, rules]
  - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'
    changes: 
    - 'src/recommendationservice/**/*'
    variables:
      MICROSERVICE: recommendationservice    

build-shippingservice:
  extends: build
  rules:
  - !reference [.build_rules, rules]
  - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'
    changes: 
    - 'src/shippingservice/**/*'
    variables:
      MICROSERVICE: shippingservice


container_scanning:    
    variables:
        CI_APPLICATION_REPOSITORY: "${CI_REGISTRY_IMAGE}/emailservice-${CI_COMMIT_REF_SLUG}"
    rules:
    - when: never

container_scanning-emailservice:
  stage: test
  extends: 
  - build-emailservice
  - container_scanning

container_scanning-adservice:
  extends: 
  - build-adservice
  - container_scanning

container_scanning-cartservice:
  extends: 
  - build-cartservice
  - container_scanning

container_scanning-checkoutservice:
  extends: 
  - build-checkoutservice
  - container_scanning

container_scanning-currencyservice:
  extends: 
  - build-currencyservice
  - container_scanning

container_scanning-frontend:
  extends: 
  - build-frontend
  - container_scanning

container_scanning-paymentservice:
  extends: 
  - build-paymentservice
  - container_scanning

container_scanning-productcatalogservice:
  extends: 
  - build-productcatalogservice
  - container_scanning

container_scanning-recommendationservice:
  extends: 
  - build-recommendationservice
  - container_scanning

container_scanning-shippingservice:
  extends: 
  - build-shippingservice
  - container_scanning


code_intelligence_go:
  rules:
  - when: never

code_intelligence_go-productcatalogservice:
  extends: 
  - build-productcatalogservice
  - code_intelligence_go


code_intelligence_go-shippingservice:
  extends: 
  - build-shippingservice
  - code_intelligence_go

code_intelligence_go-checkoutservice:
  extends: 
  - build-checkoutservice
  - code_intelligence_go
  